# LifeGE

Source codes for Game of Life Genetic Evolution Network http://LifeGE.NET

HTML/JS-code is covered by Lesser GPL v2.1

C-code is covered by Affero GPL v3

https://gitlab.com/shaos/lifege

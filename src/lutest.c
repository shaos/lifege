/* 25-APR-2020 Alexander A Shabarshin <me@shaos.net

Build: gcc -O2 lutest.c -o lutest

Results from my AMD64 machine:

FFFFFF68 2^1 * 2147483647 -> 5s
AAAAAA18 2^2 * 2147483647 -> 6s
BFFFFFC2 2^3 * 2147483647 -> 5s
0000004B 2^4 * 2147483647 -> 6s
5C87B6EE 2^5 * 2147483647 -> 5s
DB6DB6B0 2^6 * 2147483647 -> 6s
1ED7E6C8 2^7 * 2147483647 -> 5s
CB4F4C26 2^8 * 2147483647 -> 6s
7E567055 2^9 * 2147483647 -> 6s
E721A4D4 2^10 * 2147483647 -> 5s
594CD3B9 2^11 * 2147483647 -> 6s
EC14A96A 2^12 * 2147483647 -> 6s
6551305A 2^13 * 2147483647 -> 5s
7C903FFD 2^14 * 2147483647 -> 6s
64DE1F79 2^15 * 2147483647 -> 15s
AFB68EBD 2^16 * 2147483647 -> 17s
CE4B0F09 2^17 * 2147483647 -> 18s
902C7F06 2^18 * 2147483647 -> 19s
AB68549C 2^19 * 2147483647 -> 28s
D0ECA61B 2^20 * 2147483647 -> 34s
A5FF4DB7 2^21 * 2147483647 -> 73s
CA388996 2^22 * 2147483647 -> 160s

*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#define N INT_MAX
#define T char
#define M 0xFF

int main(int argc, char **argv)
{
 int i,j,k,l,n,m;
 unsigned int s;
 time_t t1,t2;
 T *a;
 if(argc<2) n = 22;
 else n = atoi(argv[1]);
 if(n==0) return -1;
 for(l=1;l<=n;l++)
 {
  k = 1<<l;
  m = k - 1;
  a = (T*)malloc(k*sizeof(T));
  if(a==NULL) return -2;
  srand(time(NULL));
  for(i=0;i<k;i++) a[i]=rand()+(rand()<<15);
  i = s = 0;
  t1 = time(NULL);
  for(j=0;j<N;j++)
  {
    s += ((unsigned)a[i])&M;
    i = (i+j+j+s)&m;
  }
  t2 = time(NULL);
  printf("%8.8X 2^%i * %i -> %is\n",s,l,N,(int)(t2-t1));
  free(a);
 }
 return 0;
}

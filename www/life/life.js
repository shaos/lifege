//  life.js - Conway's Game of Life in web-browser for LifeGE.net
//
//  Copyright (C) 2009.2020  Alexander A. Shabarshin <me@shaos.net>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

function copyright() {
 return "life.js v1.2 (c) 2009,2020 Alexander A. Shabarshin <me@shaos.net>";
}
// History:
// v1.0 (2009-01-24) initial version with one table per page
// v1.1 (2009-01-24) working table identified by width and height
// v1.2 (2020-04-17) optional connected sides of the map (torus) + extra colors

var life_torus = 0;
var life_fixcolor = 0;

function create_name(height,width,row,col) {
 return height + "_" + width + "_" + row + "_" + col;
}

function neighbour(height,width,row,col,val) {
 if(row>=0 && row<height && col>=0 && col<width) {
  var el = document.getElementById(create_name(height,width,row,col));
  var nei = parseInt(el.innerHTML);
  nei = nei + val;
  el.innerHTML = nei;
 }
}

function neighbours(height,width,row,col,val) {
 var i,j;
 for(j=-1;j<=1;j++) {
 for(i=-1;i<=1;i++) {
   if(i!=0 || j!=0) {
     if(life_torus==0) {
       neighbour(height,width,row+j,col+i,val);
     } else {
       var r = row+j;
       var c = col+i;
       if(r<0) r=height-1;
       if(r>=height) r=0;
       if(c<0) c=width-1;
       if(c>=width) c=0;
       neighbour(height,width,r,c,val);
     }
   }
 }}
}

function setcolor(height,width,row,col) {
 if(row>=0 && row<height && col>=0 && col<width) {
  var el = document.getElementById(create_name(height,width,row,col));
  if(el.bgColor=="red" || el.bgColor=="blue" || el.bgColor=="green") {
    el.bgColor="white";
    neighbours(height,width,row,col,-1);
  }
  else {
    el.bgColor="blue";
    neighbours(height,width,row,col,1);
  }
 }
}

function lifestep(height,width) {
 var i,j;
 // first pass
 for(j=0;j<height;j++){
 for(i=0;i<width;i++){
  var el = document.getElementById(create_name(height,width,j,i));
  var nei = parseInt(el.innerHTML);
  if((el.bgColor=="blue" || el.bgColor=="green") && (nei<2 || nei>3)) {
    // mark as dead
    el.bgColor="red";
  }
  if(el.bgColor!="blue" && el.bgColor!="green" && nei==3) {
    // mark as born
    el.bgColor="purple";
  }
 }}
 // second pass
 for(j=0;j<height;j++){
 for(i=0;i<width;i++){
  var el = document.getElementById(create_name(height,width,j,i));
  if(el.bgColor=="red" || el.bgColor=="purple") {
    // remove (if red) or add (if purple)
    setcolor(height,width,j,i);
  }
  if(life_fixcolor) {
    if(el.bgColor=="green") {
      el.bgColor="blue";
    }
    if(el.bgColor=="yellow") {
      el.bgColor="white";
    }
  }
 }}
}

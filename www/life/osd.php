<htm>
<script type="text/javascript" src="/life/life.js"></script>
<?php
echo "<body>\n";
$sz = 16;
$id = 0;
$id2 = 0;
$id3 = 0;
if(isset($_GET['hex']))
{
  $hex = $_GET['hex'];
  if(strlen($hex)<=15)
  {
    $id = hexdec($hex);
  }
  else
  {
    while(strlen($hex)<45)
    {
      $hex = '0'.$hex;
    }
    if(strlen($hex)!=45)
    {
      echo "ERROR";
      error(1);
    }
    $hexa = str_split($hex,15);
    $id3 = hexdec($hexa[0]);
    $id2 = hexdec($hexa[1]);
    $id  = hexdec($hexa[2]);
  }
}
if(!isset($_GET['cat']))
{
   echo "ERROR";
   error(2);
}
$cat = $_GET['cat'];
if($cat < 3 || $cat > 13 || (((int)($cat))&1)==0)
{
   echo "ERROR";
   error(3);
}
if($id3!=0)
{
  $m = $id3;
  $len = 120;
}
else if($id2!=0)
{
  $m = $id2;
  $len = 60;
}
else
{
  $m = $id;
  $len = 0;
}
while($m>0)
{
  $len = $len + 1;
  $m = $m >> 1;
}
$m = (int)($len/$cat);
if($len%(int)$cat)
{
  $m = $m + 1;
}
$len = $m;
echo "<h1>Object OSD".$cat."#".strtoupper(dechex($id))."</h1>\n";
echo "<font face=fixed>\n";
echo "<table border=1 cellspacing=0 cellpadding=0><tr>\n";
for($row=0;$row<$sz;$row++){
for($col=0;$col<$sz;$col++){
 echo "<td id=".$sz."_".$sz."_".$row."_".$col." align=center width=18>0</td>\n";
}
echo "</tr><tr>\n";
}
echo "</tr></table><p>\n";
echo "<INPUT TYPE=\"button\" NAME=\"step\" VALUE=\"Step\" onClick=\"lifestep(".$sz.",".$sz.")\">\n";
echo "<p>Shaos's classificator of Orthogonal Striped Defects (OSD) for Conway's Game of Life (not all such objects yet identified)\n";
echo "<p>osd.php v2020.04.22 (source code: <a href=\"https://gitlab.com/shaos/lifege\" target=\"_blank\">https://gitlab.com/shaos/lifege</a>)\n";
echo "</body>\n";
echo "<script type=\"text/javascript\">\n";
echo "var temp = 0;\n";
echo "life_torus = 1;\n";
echo "life_fixcolor = 1;\n";
$l = 7+($len>>1);
$i = (int)$id;
$j = (int)$cat;
$j = 7-($j>>1);
if($cat==3 || $cat==7 || $cat==11)
{
  $j = $j - 1;
}
$io = 0;
for($col=$l;$col>=$l-$len+1;$col--)
{
  $o = 0;
  for($row=$j;$row<$j+$cat;$row++)
  {
    if(($i&1)^$o)
    {
       echo "setcolor(".$sz.",".$sz.",".$row.",".$col.");\n";
       echo "document.getElementById(create_name(".$sz.",".$sz.",".$row.",".$col.")).bgColor=\"green\";\n";
    }
    else
    {
       echo "document.getElementById(create_name(".$sz.",".$sz.",".$row.",".$col.")).bgColor=\"yellow\";\n";
    }
    $i = $i>>1;
    $io = $io + 1;
    if($i==0)
    {
       if($io==60)
       {
          $i = $id2;
       }
       else if($io==120)
       {
          $i = $id3;
       }
    }
    $o = $o + 1;
    if($o==2) $o=0;
  }
}
for($row=0;$row<$sz;$row+=2){
for($col=0;$col<$sz;$col++){
    echo "temp = document.getElementById(create_name(".$sz.",".$sz.",".$row.",".$col.")).bgColor;\n";
    echo "if(temp!=\"green\"&&temp!=\"yellow\"){setcolor(".$sz.",".$sz.",".$row.",".$col.");}\n";
}}
echo "</script>\n";
?>
</html>
